package com.newflypig.jblog.util;

import java.util.Properties;
import java.util.Set;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

/**
 *	复写Spring框架中的 PropertyPlaceholderConfigurer
 *	赋予其读取加密字符串的功能
 *	@author newflypig
 *	time：2015年12月5日
 *
 */
public class EncryptPropertyPlaceholderConfigurer extends PropertyPlaceholderConfigurer {

	@Override
	protected void processProperties(ConfigurableListableBeanFactory beanFactoryToProcess, Properties props) throws BeansException {
		System.out.println("=================EncryptPropertyPlaceholderConfigurer setting successful!==================");  
       
        /**
         * 遍历key，通过key遍历value，对value进行正则匹配
         * 将匹配的值删"ENC(",再删")",继续对剩下的字符串进行DES解密
         */
        Set<Object> keys=props.keySet();
        for(Object o:keys){
        	String key=(String)o;
        	String value = props.getProperty(key);
        	
            if (value != null && value.matches("ENC\\([\\w+=]+\\)")) {            	
                props.setProperty(key, DESUtil.decryptString(value.substring(4,value.length()-1)));              
            }
        }
        
		super.processProperties(beanFactoryToProcess, props);
	}
}
