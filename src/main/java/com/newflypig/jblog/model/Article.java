package com.newflypig.jblog.model;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.newflypig.jblog.util.BlogUtils;

/**
 * Article entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "article", catalog = "jblog")
public class Article implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9169337066300818107L;
	// Fields

	private Integer articleId;
	private Timestamp date=new Timestamp(new Date().getTime());
	private String text;
	private String title;
	private Integer rate;
	private Integer support;
	private Integer against;
	private Set<Comment> comments = new HashSet<Comment>(0);
	private Set<Category> categories = new LinkedHashSet<Category>(0);

	// Constructors

	/** default constructor */
	public Article() {
	}
	
	public Article(Integer articleId) {
		this.articleId=articleId;
	}

	/** full constructor */
	public Article(Timestamp date, String text, String title, Integer rate, Integer support, Integer against, Set<Comment> comments,
			Set<Category> categories) {
		this.date = date;
		this.text = text;
		this.title = title;
		this.rate = rate;
		this.support = support;
		this.against = against;
		this.comments = comments;
		this.categories = categories;
	}
	
	public Article(Integer articleId,Date date,String text,String title,Integer rate,Integer support,Integer against){
		this.articleId=articleId;
		this.date=new Timestamp(date.getTime());
		this.text=text;
		this.title=title;
		this.rate=rate;
		this.support=support;
		this.against=against;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "article_id", unique = true, nullable = false)
	public Integer getArticleId() {
		return this.articleId;
	}

	public void setArticleId(Integer articleId) {
		this.articleId = articleId;
	}

	@Column(name = "date", length = 19)
	public Timestamp getDate() {
		return this.date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	@Column(name = "text", length = 65535)
	public String getText() {
		return this.text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Column(name = "title", length = 120)
	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = BlogUtils.html(title);
	}

	@Column(name = "rate")
	public Integer getRate() {
		return this.rate;
	}

	public void setRate(Integer rate) {
		this.rate = rate;
	}

	@Column(name = "support")
	public Integer getSupport() {
		return this.support;
	}

	public void setSupport(Integer support) {
		this.support = support;
	}

	@Column(name = "against")
	public Integer getAgainst() {
		return this.against;
	}

	public void setAgainst(Integer against) {
		this.against = against;
	}
	
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "article")
	@OrderBy("date DESC")
	public Set<Comment> getComments() {
		return this.comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

	@ManyToMany(fetch=FetchType.LAZY)
	@JoinTable(name="article_category",
		joinColumns={@JoinColumn(name="article_id")},
		inverseJoinColumns={@JoinColumn(name="category_id")}
	)
	@OrderBy("categoryId ASC")
	public Set<Category> getCategories() {
		return this.categories;
	}

	public void setCategories(Set<Category> categories) {
		this.categories = categories;
	}
	
	
	public void buildCategories(String strCategories){
		if(strCategories!=null && !"".equals(strCategories)){
			for(String value:strCategories.split(",")){
				Integer categoryId=Integer.parseInt(value);
				Category c=new Category();
				c.setCategoryId(categoryId);
				this.getCategories().add(c);
			}
		}
	}
	
	private Pager<Comment> pagerComments;

	@Transient
	public Pager<Comment> getPagerComments() {
		return pagerComments;
	}

	public void setPagerComments(Pager<Comment> pagerComments) {
		this.pagerComments = pagerComments;
	}	
}