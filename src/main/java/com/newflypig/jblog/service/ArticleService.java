package com.newflypig.jblog.service;

import java.util.List;

import com.newflypig.jblog.model.Article;

/**
 *	定义关于Article类的特别服务方法
 *	time：2015年11月28日
 *
 */
public interface ArticleService extends BaseService<Article>{
	/**
	 * 倒序排列所有文章
	 * @return
	 */
	public List<Article> findAllDesc();	
}
