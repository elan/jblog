package com.newflypig.jblog.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.newflypig.jblog.dao.BaseDAO;
import com.newflypig.jblog.dao.SystemDAO;
import com.newflypig.jblog.model.System;
import com.newflypig.jblog.service.SystemService;

@Service("systemService")
public class SystemServiceImpl extends BaseServiceImpl<System> implements SystemService{

	@Resource(name="systemDao")
	private SystemDAO systemDao;
	
	@Override
	protected BaseDAO<System> getDao() {
		return this.systemDao;
	}

	@Override
	@Transactional(propagation=Propagation.NEVER)
	public System loginByUsernamePwd(String userName, String password) {
		return this.systemDao.loginByUsernamePwd(userName, password);
	}
	
	@Transactional(propagation=Propagation.NEVER)
	public System getSystem(){
		return this.systemDao.getSystem();
	}
}
