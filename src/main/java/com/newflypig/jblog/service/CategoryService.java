package com.newflypig.jblog.service;

import com.newflypig.jblog.model.Category;


/**
 *	定义关于Category类的特别服务方法
 *	time：2015年11月28日
 *
 */
public interface CategoryService extends BaseService<Category>{

}
