<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language="java" import="com.newflypig.jblog.model.Category"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/resources/css/common.css">
<title>Category show</title>
</head>
<body>
	<h1>ID:&nbsp;${category.categoryId }</h1>
	<h1>Title:&nbsp;${category.title }</h1>
	<h1>Number:&nbsp;${category.number }</h1>
</body>
</html>