<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/common.css">
<title>Insert title here</title>
<style type="text/css">
#login-box{
	width:225px;
	margin:200px auto;
	text-align: center;
}
</style>
</head>
<body>
	<form method="post" id="login-box">
		<input type="text" name="userName" placeholder="请输入用户名"><br/>
		<input type="password" name="userPwd" placeholder="请输入密码"><br/>		
		<%if(((Integer)session.getAttribute("post_num"))>1){ %>
		<div style="height: 50px;">
			<span style="width:90px;height:50px;"><input type="text" name="captcha" placeholder="请输入验证码" style="width:90px;margin:15px 0px 15px 0px;"></span>
			<span style="width:100px;height:50px;"><img id="captchaImg" title="点击重置" height="50px" width="100px" src="/captcha-image"></span>
		</div>
		<script type="text/javascript">
			document.getElementById("captchaImg").onclick=function(e){
				e.target.src="captcha-image?m="+Math.random();
			};
		</script>
		<%}%>
		<input type="submit" value="submit">
		<a href="<%=request.getContextPath()%>">返回</a>
	</form>
</body>
</html>